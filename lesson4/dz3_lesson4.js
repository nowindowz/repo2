
//Все предыдущий задания на циклы - написать с помощью циклов for in и/или for of


console.log('С помощью конструкций for in / for of');

var t ='ропот';
var res ='';

for (let index in t) {
    res = t[index] + res;
};

console.log('Переменная содержит в себе строку. Вывести строку в обратном порядке');
console.log('исходная строка: '+t+' в обратном порядке: '+res);

function dz1_lesson2(t) {
    var res = '';
    for (let index in t) {
      res = ""+  t[index] + res;
    };
  
    return res;
};

t ='Привет JS';
console.log('исходная строка: '+t+' в обратном порядке: '+dz1_lesson2(t));


//
function unique(n, m, nValues) {
    let result = [];

    let max;
    let min;

    if(n<m){
        max =m;
        min =n;
    }else{
        max =n;
        min =m;
    };

    if((max + 1 - min) < nValues){
        return result;
    };

    i =0;
    while (i < nValues) {
        x = Math.floor(Math.random() * (max+1 - min) + min);
        // console.log(x);
        if(result.indexOf(x) == -1){
            result.push(x);
            i +=1;
        };
    };
  
    return result;
  };

result =unique(-10, 15, 20);
//console.log(result);

function PercentRatio(m){
    let numNegative =0;
    let numPositive =0;
    let num0 =0;

    for (var i of result) {
        if (i < 0){
            numNegative +=1;
        }else if (i > 0){
            numPositive +=1;
        }else{
            num0 +=1;
        };
    };

    console.log('Процент отрицательных в массиве: '+numNegative * 100 / m.length +'%');
    console.log('Процент положительных в массиве: '+numPositive * 100 / m.length +'%');
    console.log('Процент нулей в массиве: '+num0 * 100 / m.length +'%');
}; 

PercentRatio(result);

  
