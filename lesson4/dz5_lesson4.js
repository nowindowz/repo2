// Написать рекурсивную функцию которая выводит абсолютно все элементы
// ассоциативного массива (объекта) - любого уровня вложенности

  
let obj = {
    xxx: 1,
    b: {
        a: 2,
        b: 3,
    },
    d: {
        a: 6,
        trtrtr: "hello!",
        s: {
           a:77,
           b: 'Preved', 
           onemore: [{
                a: 333,
                b: 777,
                c: 123,
           },
            {
                a: 747,
                b: 'JS cool',
            }],
        },
    },
    f: {
       ggg: [1, 2, 3],
       b: 75694 
    },
};

function show(ob){
    for(let i in ob){
        if(typeof ob[i] === "object" || typeof ob[i] === "array"){
            show(ob[i]);
        }else{
            console.log(ob[i]);
        };
    };
};

show(obj);