//  Написать функцию которая генерирует массив случайных значений,
//  таким образом что все элементы результирующего массива являются уникальными.
//  Генерациями происходит в рамках чисел от N до M, где N, M - могут быть как положительные так и отрицательными, 
//  и еще одним параметром количество значений которые нужно сгенерировать.
//  Если количество генерируемых значений больше чем чисел в диапазоне - отдавать пустой массив.

function unique(n, m, nValues) {
    let result = [];

    let max;
    let min;

    if(n<m){
        max =m;
        min =n;
    }else{
        max =n;
        min =m;
    };

    if((max + 1 - min) <nValues){
        return result;
    };

    i =0;
    while (i < nValues) {
        x = Math.floor(Math.random() * (max+1 - min) + min);
        // console.log(x);
        if(result.indexOf(x) == -1){
            result.push(x);
            i +=1;
        };
    };
  
    return result;
  };

  result =unique(-1, 8, 10);
  console.log(result);

  
