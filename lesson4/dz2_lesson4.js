// Использовать функцию из предыдущего задания чтобы получить массив
//  из нужного количества значений. Найти процентное соотношение отрицательных,
//   положительных и нулевых элементов массива.


function unique(n, m, nValues) {
    let result = [];

    let max;
    let min;

    if(n<m){
        max =m;
        min =n;
    }else{
        max =n;
        min =m;
    };

    if((max + 1 - min) < nValues){
        return result;
    };

    i =0;
    while (i < nValues) {
        x = Math.floor(Math.random() * (max+1 - min) + min);
        // console.log(x);
        if(result.indexOf(x) == -1){
            result.push(x);
            i +=1;
        };
    };
  
    return result;
  };

  result =unique(-10, 15, 20);
  console.log(result);

function PercentRatio(m){
    let numNegative =0;
    let numPositive =0;
    let num0 =0;

    for(i = 0; i < m.length; i++){
        if (m[i] < 0){
            numNegative +=1;
        }else if(m[i] > 0){
            numPositive +=1;
        }else{
            num0 +=1;
        };

    };

    console.log('Процент отрицательных в массиве: '+numNegative * 100 / m.length +'%');
    console.log('Процент положительных в массиве: '+numPositive * 100 / m.length +'%');
    console.log('Процент нулей в массиве: '+num0 * 100 / m.length +'%');

}; 


PercentRatio(result);

  
