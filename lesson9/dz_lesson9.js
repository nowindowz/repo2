

window.onload = function () {

    /*При каждом действии удаления или добавления студентов нужно пересчитывать статистику
    средней оценки в разрезе каждого курса и подсчета количества неактивных студентов
    и изменять соответствующее содержимое.*/

    var Students = [];

    //Добавить еще одно свойство для студента - email адрес

    // Вывести mail адрес на ряду в списке студентов
    // Так же организовать возможность изменения по аналогии с именем

    // Написать регулярное выражение проверки введенных данных email
    // Написать валидацию проверку ввода данных - курс это любое целое число от 1 до 5,
    // email - с помощью регулярки,
    // имя может содержать только буквы не более 15 символов

    //Изменять данные физически тогда и только тогда, когда будут введены корректные

    // Добавить еще одно свойство: date - дата создания студента,
    // брать текущее время клиента и записывать наряду с каждым студентом,
    //  выводить дату в списке со студентами - ее нельзя изменить.


    function isValid(typeField , valField){

        if (typeField == 'email'){

            let re = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

            if (re.test(valField) === false ){
                return 'Не верно указан email';
            };

        } else if (typeField == 'name'){

            // let re = /[a-zA-Z]+/;
            let re = /^([а-яё]+|[a-z]+)$/i;

            if (re.test(valField) === false ){
                return 'поле name содержит неверные символы!';
            } else if (valField.length > 15){
                return 'поле name содержит больше 15 символов!';
            };           

        }  else {
            if ((valField % 1) !== 0) {
                return 'Число не целое!'
            } else if (valField > 5){
                return 'Число больше 5!'
            } else if (valField < 1){
                return 'Число меньше 1!'
            };
        };

        return '';

    };

    function Student(name, estimate, course, active, email, date, id) {
        this.name = name;
        this.estimate = estimate;
        this.course = course;
        this.active = active;
        this.email = email;
        this.date = date;
        this.id = id;

    };

    function sendStudent (method, url, callback, obj){
        let self = this;
    
        let data = obj || false;
    
        var xhr = new XMLHttpRequest();
    
        xhr.open(method, url, true);
    
        xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
        xhr.setRequestHeader("X-Authorization-Token", "bde23f58-341b-11eb-a483-f1c3feb07438");
    
        if(data){
            xhr.send(JSON.stringify(data));
        }else{
            xhr.send();
        };
        
        xhr.onreadystatechange = function() {

            if (xhr.readyState != 4) return;
            if (xhr.status == 200) {
                let response = JSON.parse(xhr.responseText);
                callback(response);
            };
        };

    };


    // Написать ajax запрос получения данных по студентам из сервера (сервис будет предоставлен)

    function getAll(){

        // Students.push(new Student('Ivan', 5, 1, true,  'ffff@gmail.com', ''));
        
        function callback(response){

            if(!response.is_error){

                let newStudents = response.students;

                if (newStudents.length >0 ){

                    // console.log(newStudents);

                    for(let i = 0; i < newStudents.length; i++){
                        let dateNew = new Date();
                        
                        Students.push(new Student(newStudents[i].first_name, newStudents[i].estimate, newStudents[i].course, newStudents[i].is_active,  '', dateNew.toISOString(), newStudents[i].id));
                    }; 

                    // console.log(Students.length);
                    renderStudents();

                } else {
                    console.log('читаем из localstorage');
                    var localSt =localStorage.getItem("myKey");
                    if (localSt != undefined ){
                        Students = JSON.parse(localSt); 
                    };

                };

            };
            
        };

        sendStudent("GET", 'https://evgeniychvertkov.com/api/student/', callback);
      
    };

    getAll();

    function addStudent(student){
        // let self = this;
        function callback(response){
            if(!response.is_error){

                // Students.push(response.student);
                // renderStudents();
            }
        }
        sendStudent("POST", 
       'https://evgeniychvertkov.com/api/student/', 
        callback, student);
    };

    function removeOnServer(index){
        
        function callback(response){
            // if(!response.is_error){
            //     self.remove(index);
            //     self.render();
            // }
        }

        console.log(Students);
    
        sendStudent("DELETE", 
        'https://evgeniychvertkov.com/api/student/' + Students[index].id + '/', 
        callback);
    };

    function updateStudent(student){

        

        function callback(response){
            console.log(response);

            if(!response.is_error){
            // self.render();
            } else {
                console.log(response);
            };
            ;
        };

        // console.log(student);

        sendStudent("PUT", 'https://evgeniychvertkov.com/api/student/', callback, student);
    };

    // Все данные хранить в localStorage касательно студентов
    // При открытии страницы выбирать из localstorage и показывать список студентов и статистику соответственно
    

    // ********
    // if (localSt == undefined ){
    //     // let morning = new Date();
    //     // morning.setHours(0);

    //     // Students.push(new Student('Ivan', 5, 1, true,  'test@gmail.com', morning));
    //     // Students.push(new Student('Ivan', 4, 3, true,  'student@ukr.net', new Date()));
    //     // Students.push(new Student('Ivan', 2, 4, false, 'test@.com.ua', new Date()));
    //     // Students.push(new Student('Ivan', 5, 2, true,  'test2020@yahoo.com', new Date()));
        
    // } else {
    //     Students = JSON.parse(localSt); 
    // };

    function writeLocal() {
        let serialObj = JSON.stringify(Students); //сериализуем его
        localStorage.setItem("myKey", serialObj);
        // returnObj = JSON.parse(localStorage.getItem("myKey"));
    };

    // writeLocal();

    //*******

    function color(estimate){
        let color ='red';

        if (estimate > 3 && estimate <= 4){
            color ='yellow';  
        }else if(estimate > 4){
            color ='green';
        };

        return color;
    };

    function renderStudents() {

        // Если localstorage не имеет никаких данных в себе, 
        // показывать что ничего нет 
        // при добавлении из формы нового студента - 
        // данные должны попасть и сохраниться в localstorage, 
        // при следующем открытии страницы - уже подтянуться сохраненные данные

        writeLocal();

        let list = document.querySelector(".list");
        list.innerHTML = "";

        if (Students.length == 0 ){
            list.innerHTML = 'Студенты не найдены';
            return '';
        };

        let ulStudents = document.createElement("UL");
        for(let i = 0; i < Students.length; i++){
            let liStudents = document.createElement("LI");
            let liSpan = document.createElement("SPAN");
            let liSpanName = document.createElement("SPAN");
            let liSpanEstimate = document.createElement("SPAN");
            let liSpanCourse = document.createElement("SPAN");
            let liSpanEmail = document.createElement("SPAN");
            let liSpanDate = document.createElement("SPAN");

            let buttonSt = document.createElement("BUTTON");
            buttonSt.innerHTML = 'X';

            // liSpan.innerHTML = 'name:'+Students[i].name+' course: '+Students[i].course +' estimate: '+Students[i].estimate+' active: '+Students[i].active+'  ';
            liSpanName.innerHTML = ' name: '+Students[i].name+' '; 
            liSpanEstimate.innerHTML = ' estimate: '+Students[i].estimate+' '; 
            liSpanCourse.innerHTML = ' course: '+Students[i].course+' '; 
            liSpanEmail.innerHTML = ' email адрес: '+Students[i].email+' '; 
            liSpanDate.innerHTML = ' date: '+Students[i].date+' '; 

            liSpan.style.width = '500px';
            liSpan.className = 'lispan';
            liSpanName.className = 'lispan';
            liSpanEstimate.className = 'lispan';
            liSpanCourse.className = 'lispan';
            liSpanEmail.className = 'lispan';
            liSpanDate.className = 'lispan';
           
            // В ряде предыдущих заданий - выделять красным цветом тех студентов которые
            // имеют оценку 3 и менее. которые от 3 до 4  - желтым и которые 4 и выше - зеленым.

            let color ='red';
            
            if (Students[i].estimate > 3 && Students[i].estimate <= 4){
                color ='yellow';
                liSpanName.style.backgroundColor ='#6a9bdb';
                liSpanEstimate.style.backgroundColor ='#6a9bdb';
                liSpanCourse.style.backgroundColor ='#6a9bdb';
                liSpanEmail.style.backgroundColor ='#6a9bdb';
            }else if(Students[i].estimate > 4){
                color ='green';
            };

            liSpanName.style.color =color;
            liStudents.style.color =color;

            liStudents.appendChild(liSpanName);
            liStudents.appendChild(liSpanEstimate);
            liStudents.appendChild(liSpanCourse);
            liStudents.appendChild(liSpanEmail);


            // liStudents.appendChild(liSpan);
            liStudents.appendChild(buttonSt);
            liStudents.appendChild(liSpanDate);

            // Добавить для каждого студента иконку по нажатию на которую студент
            // переводится в статус неактивный из активного и наоборот - 
            // при этом для двух состояний иконки тоже должны быть разными и изменять

            let pic = document.createElement("IMG");
            if (Students[i].active) {
                pic.src = "../repo2/lesson7/active.png";
            }else{
                pic.src = "../repo2/lesson7/notactive.png";
            };
            
            pic.style.height = '30px';
            pic.style.width = '30px';
            liStudents.appendChild(pic);

            (function (i){
                buttonSt.addEventListener("click", function(event){
                    removeOnServer(i);
                    Students.splice(i, 1);  
                    renderStudents();
                });

                pic.addEventListener("click", function(event){
                    Students[i].active = !Students[i].active;
                    renderStudents();
                });

                // По нажатие на имя студента - удалять имя, вместо имени показывать форму ввода - 
                // по нажатию на ENTER сохранять новое имя для этого студента,
                // удалять форму ввода и выводить в списке новое имя студента

                // По аналогии предыдущего пункта сделать тоже самое с номером курса
                // и с оценкой студента. Не забыть что при изменении оценки 
                // статистика также должна быть пересчитана и выведена новая статистика.


                liSpanName.addEventListener("click", function(event){
                    let input = document.createElement("INPUT");
                    input.type = "text";
                    input.className = "input";
                    liSpanName.innerHTML = '';
                    liSpanName.appendChild(input);

                    input.addEventListener("keydown", function(event){
                        if(event.keyCode === 13){
                            // Students[i].name = event.target.value;
                            // renderStudents();
                            let newName = event.target.value;
                            let validation = isValid('name' , newName);

                            if (validation === ''){
                                Students[i].name = newName;
 

                                updateStudent({
                                    first_name: Students[i].name,
                                    course: Students[i].course,
                                    estimate: Students[i].estimate,
                                    is_active: Students[i].active
                                });


                                renderStudents();
                            } else {
                                alert(validation)
                            };

                        }
                     });

                     input.focus();
                });
                liSpanEstimate.addEventListener("click", function(event){
                    let inputEstimate = document.createElement("INPUT");
                    inputEstimate.type = "number";
                    inputEstimate.className = "input";
                    liSpanEstimate.innerHTML = '';
                    liSpanEstimate.appendChild(inputEstimate);

                    inputEstimate.addEventListener("keydown", function(event){
                        if(event.keyCode === 13){
                            Students[i].estimate = Number(event.target.value);

                            updateStudent({
                                first_name: Students[i].name,
                                course: Students[i].course,
                                estimate: Students[i].estimate,
                                is_active: Students[i].active
                            });

                            renderStudents();
                        }
                     });

                     inputEstimate.focus();
                });
                liSpanCourse.addEventListener("click", function(event){
                    let inputCourse = document.createElement("INPUT");
                    inputCourse.type = "number";
                    inputCourse.className = "input";
                    liSpanCourse.innerHTML = '';
                    liSpanCourse.appendChild(inputCourse);

                    inputCourse.addEventListener("keydown", function(event){
                        if(event.keyCode === 13){
                            // Students[i].сourse = event.target.value;

                            let newCourse = Number(event.target.value);
                            let validation = isValid('course' , newCourse);

                            if (validation === '' ){
                                Students[i].course = newCourse;

                                updateStudent({
                                    first_name: Students[i].name,
                                    course: Students[i].course,
                                    estimate: Students[i].estimate,
                                    is_active: Students[i].active
                                });

                                renderStudents();
                            } else {
                                alert(validation)
                            };                            
                        }
                     });

                     inputCourse.focus();
                });
                liSpanEmail.addEventListener("click", function(event){
                    let inputEmail = document.createElement("INPUT");
                    inputEmail.type = "text";
                    inputEmail.className = "input";
                    liSpanEmail.innerHTML = '';
                    liSpanEmail.appendChild(inputEmail);

                    inputEmail.addEventListener("keydown", function(event){
                        if(event.keyCode === 13){
                            // Students[i].email = event.target.value;
                            // renderStudents();
                            let newEmail = event.target.value;
                            let validation = isValid('email' , newEmail);

                            if (validation === ''){
                                Students[i].email = newEmail;

                                renderStudents();
                            } else {
                                alert(validation)
                            };


                        }
                     });

                     inputEmail.focus();
                });

            })(i);

            ulStudents.appendChild(liStudents);
        };

        list.appendChild(ulStudents);

        averageEstimate(list);

    };

    // renderStudents();

    function averageEstimate(list) {

        let courseEst = {};
        let courseAverageEst = {};

        let courseArr = [];
        let notActiveStudents = {};
        let notActiveAll = 0;

        //Добавить в статистику еще одно вычисление:  количество студентов которые были добавлены за последний час.
        let added =0;

        for (let i in Students) {
            if (courseArr.indexOf(Students[i].course) == -1) {
                courseArr.push(Students[i].course);
            };

            now = new Date();
            
            // if (Students[i].date.getHours() === now.getHours()){

            // console.log(now - Date.parse(Students[i].date));
            if ((now - Date.parse(Students[i].date)) <= 3600000){    
                added +=1; 
            };
         
        };
  

        for (let x = 0; x < courseArr.length; x++) {

            courseEst[courseArr[x]] = [];
            notActiveStudents[courseArr[x]] = 0;

            for (let i in Students) {
                if (Students[i].course == courseArr[x]) {
                    if (Students[i].active) {

                        courseEst[courseArr[x]].push(Students[i].estimate);

                    } else {
                        notActiveStudents[courseArr[x]] += 1;
                    };
                };
            };

            for (let key in notActiveStudents) {
                if (key == courseArr[x]) {
                    notActiveAll += notActiveStudents[key];
                };
            };

            for (let key2 in courseEst) {

                if (key2 == courseArr[x]) {

                    let courseSumEstimates = 0;
                    for (let j = 0; j < courseEst[key2].length; j++) {
                        courseSumEstimates += courseEst[key2][j];
                    };

                    if (courseEst[key2].length > 0) {
                        courseAverageEst[courseArr[x]] = courseSumEstimates / courseEst[key2].length;
                    };
                };
            };
        };

        // Аналогично как в предыдущем задании этого урока отмечать фоновым цветом 
        // вывод статистики в разрезе каждого курса касательно средней оценки

        let divEmpty = document.createElement('div');
        divEmpty.className = "alert";
        list.appendChild(divEmpty);

        let div4 = document.createElement('div');
        div4.className = "alert";
        div4.innerHTML = 'Средняя оценка по курсам: ';

        for (let index in courseAverageEst){

            let br = document.createElement('br');
            div4.appendChild(br);

            let Span = document.createElement('SPAN');
            Span.innerHTML = 'course'+index+': '+courseAverageEst[index]+' <br>';
            Span.style.backgroundColor =color(courseAverageEst[index]);
            div4.appendChild(Span);
        };

        list.appendChild(div4);

        let div5 = document.createElement('div');
        div5.className = "alert";
        div5.innerHTML = 'Не активные студенты по курсам: ';
        for (let index in notActiveStudents){
            div5.innerHTML += 'course'+index+': '+notActiveStudents[index]+' ';
        };

        list.appendChild(div5);

        let div6 = document.createElement('div');
        div6.className = "alert";
        div6.innerHTML = 'Всего не активных студентов: '+notActiveAll;
        list.appendChild(div6);

        
        let divTime = document.createElement('div');
        divTime.className = "alert";
        divTime.innerHTML = 'За последний час добавлены: '+added+' студентов';
        list.appendChild(divTime); 

    };

    let inputCheckbox = document.createElement("INPUT");
    inputCheckbox.type = "checkbox";
    inputCheckbox.id = "checkboxActive";  
    inputCheckbox.name = "checkboxActive";
    inputCheckbox.className  = "add";

    let label = document.createElement("LABEL");
    label.setAttribute("for", "checkboxActive");
    label.innerHTML = 'Active';

    let inputName = document.createElement("INPUT");
    inputName.type = "text";
    inputName.value = "";
    inputName.name = "name";
    inputName.className  = "add";
    inputName.placeholder="name";

    let inputCourse = document.createElement("INPUT");
    inputCourse.type = "number";
    inputCourse.value = "";
    inputCourse.name = "course";
    inputCourse.className  = "add";
    inputCourse.placeholder="Course";

    let inputEstimate = document.createElement("INPUT");
    inputEstimate.type = "number";
    inputEstimate.value = "";
    inputEstimate.name = "estimate";
    inputEstimate.className  = "add";
    inputEstimate.placeholder="Estimate";

    let inputEmail = document.createElement("INPUT");
    inputEmail.type = "text";
    inputEmail.value = "";
    inputEmail.name = "email";
    inputEmail.className  = "add";
    inputEmail.placeholder="e-mail адрес";

    let StudentAdd = document.getElementById("form");

    StudentAdd.appendChild(inputName);
    StudentAdd.appendChild(inputCourse);
    StudentAdd.appendChild(inputEstimate);
    StudentAdd.appendChild(inputEmail);

    StudentAdd.appendChild(inputCheckbox);
    StudentAdd.appendChild(label);

    let buttonAdd = document.createElement("BUTTON");
    buttonAdd.innerHTML = 'Добавить';
    buttonAdd.className  = "add";
    StudentAdd.appendChild(buttonAdd);

    buttonAdd.addEventListener("click", event => {

        let formData = new FormData(StudentAdd);
        let name = formData.get("name");
        let course = Number(formData.get("course"));
        let estimate = Number(formData.get("estimate"));
        let email = formData.get("email");

        let active = false;
        if(formData.get("checkboxActive") == 'on'){
            active = true;
        };


        //validate

        let allValide =true;
        let errValidatione ='';

        let validation = isValid('email' , email);
        if (validation !== ''){
            errValidatione += ' ; '+ validation;
            allValide =false;
        };
        validation = isValid('name' , name);
        if (validation !== ''){
            errValidatione += ' ; '+ validation;
            allValide =false;
        };
        validation = isValid('course' , course);
        if (validation !== ''){
            errValidatione += ' ; '+ validation;
            allValide =false;
        };

        if (allValide){

            let dateNew = new Date();
            
            Students.push(new Student(name, estimate, course, active, email, dateNew.toISOString()));

            addStudent({
                first_name: name,
                course: course,
                estimate: estimate,
                is_active: active
            });
            // writeLocal();
            renderStudents();
        } else{
            alert(errValidatione);
        };

    });

};

