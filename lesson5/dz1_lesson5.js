// Задан двумерный массив - объединить каждый внутренний массив с верхнем массивом - 
// только по уникальным значениям. Например [1,2,4[8,4,12,],[13,29,11],[0,5,3,11],5,6,7,[3,8,21],3], 
// получаем в результате: [1,2,4,8,12,13,29,11,0,5,3,6,7,21

let m = [1, 2, 4, [8, 4, 12], [13, 29, 11], [0, 5, 3, 11], 5, 6, 7, [3, 8, 21], 3];

function newArr(m) {
    let res = [];
    let arrNum =[];

    for (let i = 0; i < m.length; i++) {
        if (typeof m[i] === "object" || typeof m[i] === "array") {

            for (let j = 0; j < m[i].length; j++) {
                if (res.indexOf(m[i][j]) == -1) {
                    res.push(m[i][j]);
                };
            };

        } else {
            if (res.indexOf(m[i]) == -1) {
                res.push(m[i]);
            };
        };
    };

    return res;
};

console.log(newArr(m));