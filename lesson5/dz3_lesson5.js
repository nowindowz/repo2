/*Задан массив объектов студентов вида
 [{name: “Ivan”, estimate: 4, course: 1, active: true},
{name: “Ivan”, estimate: 3, course: 1, active: true},
{name: “Ivan”, estimate: 2, course: 4, active: false},
{name: “Ivan”, estimate: 5, course: 2, active: true}] 
 - заполнить его более большим количеством студентов. 
 Написать функцию которая возвращает: среднюю оценку студентов
  в разрезе каждого курса: {1: 3.2, 2: 3.5, 3: 4.5, 4: 3, 5: 4.5}
  с учетом только тех студентов которые активны.
  Посчитать количество неактивных студентов в разрезе каждого курса и общее количество неактивных студентов. */

let Students = [];

function Student(name, estimate, course, active) {
    this.name = name;
    this.estimate = estimate;
    this.course = course;
    this.active = active;
};

Students.push(new Student('Ivan', 4, 1, true));
Students.push(new Student('Ivan', 3, 1, true));
Students.push(new Student('Ivan', 2, 4, false));
Students.push(new Student('Ivan', 5, 2, true));

function createStudents(count) {
    for (let i = 0; i < count; i++) {
        let name = 'Ivan';
        let estimate = Math.floor(Math.random() * 5 + 1);
        let course = Math.floor(Math.random() * 5 + 1);
        let active = parseInt((Math.random() * 2)) > 0;

        Students.push(new Student(name, estimate, course, active));
    };
};

createStudents(5);

function averageEstimate() {
    let courseEst = {};
    let courseAverageEst = {};

    let courseArr = [];
    let notActiveStudents = {};
    let notActiveAll = 0;

    for (let i in Students) {
        if (courseArr.indexOf(Students[i].course) == -1) {
            courseArr.push(Students[i].course);
        };
    };

    for (let x = 0; x < courseArr.length; x++) {

        courseEst[courseArr[x]] = [];
        notActiveStudents[courseArr[x]] = 0;

        for (let i in Students) {
            if (Students[i].course === courseArr[x]) {
                if (Students[i].active) {

                    courseEst[courseArr[x]].push(Students[i].estimate);

                } else {
                    notActiveStudents[courseArr[x]] += 1;
                };
            };
        };

        for (let key in notActiveStudents) {
            if (key == courseArr[x]) {
                notActiveAll += notActiveStudents[key];
            };
        };

        for (let key2 in courseEst) {

            if (key2 == courseArr[x]) {

                let courseSumEstimates = 0;
                for (let j = 0; j < courseEst[key2].length; j++) {
                    courseSumEstimates += courseEst[key2][j];
                };

                if (courseEst[key2].length > 0) {
                    courseAverageEst[courseArr[x]] = courseSumEstimates / courseEst[key2].length;
                };
            };
        };
    };

    console.log('Массив студентов:');
    console.log(Students);

    // console.log(courseEst);
    console.log('средняя оценка по курсам:');
    console.log(courseAverageEst);

    console.log('не активные студенты по курсам:');
    console.log(courseEst);

    console.log('не активные студенты по курсам:');
    console.log(notActiveStudents);

    console.log('Всего не активных студентов:');
    console.log(notActiveAll);

};

console.log('______________________');
averageEstimate();