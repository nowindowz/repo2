// Написать функцию которая возвращает true/false 
// в зависимости от того - все ли уникальные значения в массиве
// или есть не уникальные

let mUnic = [1, 2, 4, 11, 12, 5, 6, 7, 21, 3, 22];

function isArrUnic(m) {
    let res = [];

    for (let i = 0; i < m.length; i++) {
        if (res.indexOf(m[i]) == -1) {
            res.push(m[i]);
        }else{
            return false;
        };
    };

    return true;
};

console.log(isArrUnic(mUnic));