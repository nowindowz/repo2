

window.onload = function () {

    /*    В задании из пятого урока, взять массив со студентами 
и вывести их на страницу согласно сверстанной HTML-структуре, 
рядом с каждым студентом вывести крестик - по нажатию на который студент будет удален 
(удаляется как со страницы, так и с объекта), 
если был удален последний студент написать текстовое сообщение (“Студенты не найдены”)
*/

    let Students = [];

    function Student(name, estimate, course, active) {
        this.name = name;
        this.estimate = estimate;
        this.course = course;
        this.active = active;
    };

    Students.push(new Student('Ivan', 4, 1, true));
    Students.push(new Student('Ivan', 3, 1, true));
    Students.push(new Student('Ivan', 2, 4, false));
    Students.push(new Student('Ivan', 5, 2, true));

    function createStudents(count) {
        for (let i = 0; i < count; i++) {
            let name = 'Ivan';
            let estimate = Math.floor(Math.random() * 5 + 1);
            let course = Math.floor(Math.random() * 5 + 1);
            let active = parseInt((Math.random() * 2)) > 0;

            Students.push(new Student(name, estimate, course, active));
        };
    };

    createStudents(5);

    // console.log('Массив студентов:');
    // console.log(Students);


    function renderStudents() {

        let list = document.querySelector(".list");
        list.innerHTML = "";

        if (Students.length == 0 ){
            list.innerHTML = 'Студенты не найдены';
            return '';
        };

        let ulStudents = document.createElement("UL");
        for(let i = 0; i < Students.length; i++){
            let liStudents = document.createElement("LI");
            let buttonSt = document.createElement("BUTTON");
            buttonSt.innerHTML = 'X';

            liStudents.innerHTML = 'name:'+Students[i].name+' course: '+Students[i].course +' estimate: '+Students[i].estimate+' active: '+Students[i].active+'  ';
            liStudents.appendChild(buttonSt);

            (function (i){
                buttonSt.addEventListener("click", function(event){
                    Students.splice(i, 1);
                    renderStudents();

                });
            })(i);

            ulStudents.appendChild(liStudents);
        };

        list.appendChild(ulStudents);

        averageEstimate(list);

    };

    renderStudents();


    /* Вывести статистику средних оценок в разрезе курса
    и статистику по количеству неактивных студентов 
    в разрезе каждого курса и общее количество неактивных студентов */

    function averageEstimate(list) {

        // this.list =list;

        let courseEst = {};
        let courseAverageEst = {};

        let courseArr = [];
        let notActiveStudents = {};
        let notActiveAll = 0;

        for (let i in Students) {
            if (courseArr.indexOf(Students[i].course) == -1) {
                courseArr.push(Students[i].course);
            };
        };

        for (let x = 0; x < courseArr.length; x++) {

            courseEst[courseArr[x]] = [];
            notActiveStudents[courseArr[x]] = 0;

            for (let i in Students) {
                if (Students[i].course === courseArr[x]) {
                    if (Students[i].active) {

                        courseEst[courseArr[x]].push(Students[i].estimate);

                    } else {
                        notActiveStudents[courseArr[x]] += 1;
                    };
                };
            };

            for (let key in notActiveStudents) {
                if (key == courseArr[x]) {
                    notActiveAll += notActiveStudents[key];
                };
            };

            for (let key2 in courseEst) {

                if (key2 == courseArr[x]) {

                    let courseSumEstimates = 0;
                    for (let j = 0; j < courseEst[key2].length; j++) {
                        courseSumEstimates += courseEst[key2][j];
                    };

                    if (courseEst[key2].length > 0) {
                        courseAverageEst[courseArr[x]] = courseSumEstimates / courseEst[key2].length;
                    };
                };
            };
        };

        let divEmpty = document.createElement('div');
        divEmpty.className = "alert";
        list.appendChild(divEmpty);

        let div4 = document.createElement('div');
        div4.className = "alert";
        div4.innerHTML = 'Средняя оценка по курсам: ';

        for (let index in courseAverageEst){
            div4.innerHTML += 'course'+index+': '+courseAverageEst[index]+' ';
        };

        list.appendChild(div4);

        let div5 = document.createElement('div');
        div5.className = "alert";
        div5.innerHTML = 'Не активные студенты по курсам: ';
        for (let index in notActiveStudents){
            div5.innerHTML += 'course'+index+': '+notActiveStudents[index]+' ';
        };

        list.appendChild(div5);

        let div6 = document.createElement('div');
        div6.className = "alert";
        div6.innerHTML = 'Всего не активных студентов: '+notActiveAll;
        list.appendChild(div6);

    };

    //  Добавить текстовое поле ввода - ввод имени студента,
    //  поле ввода для курса, оценки и checkbox для активности студента,
    //  по нажатии на кнопку “Добавить” - студент новый добавляется в список студентов

    let inputCheckbox = document.createElement("INPUT");
    inputCheckbox.type = "checkbox";
    inputCheckbox.id = "checkboxActive";  
    inputCheckbox.name = "checkboxActive";
    inputCheckbox.className  = "add";
    // inputCheckbox.value = false;

    let label = document.createElement("LABEL");
    label.setAttribute("for", "checkboxActive");
    label.innerHTML = 'Active';

    let inputName = document.createElement("INPUT");
    inputName.type = "text";
    inputName.value = "";
    inputName.name = "name";
    inputName.className  = "add";
    inputName.placeholder="name";

    let inputCourse = document.createElement("INPUT");
    inputCourse.type = "number";
    inputCourse.value = "";
    inputCourse.name = "course";
    inputCourse.className  = "add";
    inputCourse.placeholder="Course";

    let inputEstimate = document.createElement("INPUT");
    inputEstimate.type = "number";
    inputEstimate.value = "";
    inputEstimate.name = "estimate";
    inputEstimate.className  = "add";
    inputEstimate.placeholder="Estimate";

    let StudentAdd = document.getElementById("form");

    StudentAdd.appendChild(inputName);
    StudentAdd.appendChild(inputCourse);
    StudentAdd.appendChild(inputEstimate);

    StudentAdd.appendChild(inputCheckbox);
    StudentAdd.appendChild(label);

    let buttonAdd = document.createElement("BUTTON");
    buttonAdd.innerHTML = 'Добавить';
    buttonAdd.className  = "add";
    StudentAdd.appendChild(buttonAdd);

    buttonAdd.addEventListener("click", event => {

        let formData = new FormData(StudentAdd);
        let name = formData.get("name");
        let course = formData.get("course");
        let estimate = formData.get("estimate");

        let active = false;
        if(formData.get("checkboxActive") == 'on'){
            active = true;
        };
        
        // alert(active);

        Students.push(new Student(name, estimate, course, active));

        renderStudents();
    });
};

