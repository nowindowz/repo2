// Написать функцию, которая складывает две матрицы

let matrix1 = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];

let matrix2 = [
    [5, 6, 66],
    [33, 1, 6],
    [11, 3, 17]
];


function SumMatrix(m1, m2)     
{   
    var m = m1.length;
    n = m1[0].length;
    mRes = [];

    for (var i = 0; i < m; i++)
     { mRes[ i ] = [];
       for (var j = 0; j < n; j++) mRes[ i ][j] = m1[ i ][j]+m2[ i ][j];
     }
    return mRes;
};


console.log(SumMatrix(matrix1, matrix2));