/* Удалить из массива все столбцы которые не имеют ни одного нулевого элемента 
 и сумма которых положительна - оформить в виде функции*/

let matrix6 = [
    [1, 2, 3, 0, -17],
    [5, 2, 6, 1, 4 ],
    [5, 0, 6, 1, 3 ],
    [5, 0, 6, 1, 2 ],
    [-17, 8, 1, 7, 1]
];

function removeColumn(m) {
    var columnNumbers = [];

    for (var i = 0; i < m[0].length; ++i) {
        var sum = 0;
        var nulElem =false;
        for (var j = 0; j < m.length; ++j) {
            sum += m[j][i];
            if (m[j][i] == 0){
                nulElem =true;
            };
        };
        if ((sum >= 0) && (nulElem ==false)) {
            columnNumbers.push(i); 
        };
    };

    for (var i = 0; i < m[0].length; ++i) {
        for(var x = 0; x < columnNumbers.length; ++x){
            if (columnNumbers[x] == i){
                for (var j = 0; j < m.length; ++j) {
                    // console.log('rem '+m[j][i]);  
                    m[j].splice(i,1);                
                }; 
                columnNumbers.splice(x,1);
            };
        };
    };

    if(columnNumbers.length >0){
        removeColumn(m); 
    }else{
        return;
    }; 

};

removeColumn(matrix6);
console.log(matrix6);