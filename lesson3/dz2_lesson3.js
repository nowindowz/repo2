
//
// Все скрипты которые используют в своей основе цикл - написать с помощью рекурсивных функций
//

// Переменная содержит в себе строку. Вывести строку в обратном порядке.
function dz1_lesson2(t, res, i) {
    if (res === undefined) {
        res = '';
    };
    if (i === undefined) {
        i = 0;
    };

    if (i < t.length) {
        res = t[i] + res;
        return dz1_lesson2(t, res, ++i);
    } else if (i => t.length) {
        console.log(res);
        return res;
    };
};

dz1_lesson2('Привет JS !');

console.log('**********');

//Переменная содержит в себе число. Написать скрипт который посчитает факториал этого числа.
var num = 7;

function factorial(num, res) {

    if (res === undefined) {
        res = 1;
    };

    if (num < 1) {
        return res;
    } else {
        return factorial(num - 1, res * num);
    }
}

console.log(factorial(num));
console.log('**********');

// Дано число - вывести первые N делителей этого числа нацело.

function divider(a, n, i) {

    if (n <= 0) {
        return;
    };

    if (i === undefined) {
        i = 0;
    };

    i++;

    if (a % i == 0) {
        console.log(i);
        n--;
    };
    return divider(a, n, i);

};

divider(40, 6);
console.log('**********');

// Найти сумму цифр числа которые кратны двум

function sum_krat2(num, res) {

    if (res === undefined) {
        res = 0;
    };

    a = num % 10;

    if (num <= 0) {
        return res;
    } else {
        num = (num - a) / 10;

        if (a % 2 == 0) {
            res += a;
        };

        return sum_krat2(num, res);
    }
};

sum = sum_krat2(7854282);

console.log(sum);
console.log('**********');

// Найти минимальное число которое больше 300 и нацело делиться на 17

function find_min(num) {
    if ((num % 17) == 0) {
        return num;
    } else {
        return find_min(++num);
    };
};

console.log(find_min(300));
console.log('**********');

// Заданы две переменные для двух целых чисел, найти максимальное общее значение которое нацело делит два заданных числа.

function find_max(a, b, i) {

    if (i === undefined) {
        if (a <= b) { i = a }
        else { i = b };
    };

    if ((a % i) == 0 && (b % i) == 0) {
        return i;
    } else {
        return find_max(a, b, --i);
    };
};


console.log(find_max(880, 200));
console.log('**********');
