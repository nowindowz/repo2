
// Найти номер столбца двумерного массива сумма которого является максимальной (аналогично для строки)

let matrix = [
    [1, 2, 3],
    [55, 20, 6],
    [7, 8, 139]
];

function numColumn(matrix) {
    var columnNumber = 1;
    var maxSum =0;
    for (var i = 0; i < matrix[0].length; ++i) {
        var sum = 0;
        for (var j = 0; j < matrix.length; ++j) {
            sum += matrix[j][i];
        };
        if (sum > maxSum) {
            // console.log(sum);
            maxSum =sum;
            columnNumber += i; 
        };
    };
    return columnNumber;
};

console.log('номер столбца сумма которого является максимальной '+numColumn(matrix));

let matrixOfStr = [ ['nnfn', 'ghjjh','tettest'],
                    ['ddd', 'blblbfп', '25'],
                    ['7tetstt', 'dz77', 'xxx']];

function numColumn(matrix) {
var columnNumber = 1;
var maxSum =0;

for (var i = 0; i < matrix[0].length; ++i) {
    var sum = 0;
    for (var j = 0; j < matrix.length; ++j) {
        sum += matrix[j][i].length;
    };
    if (sum > maxSum) {
        // console.log(sum);
        maxSum =sum;
        columnNumber += i; 
    };
};
return columnNumber;
};                    

console.log('номер столбца сумма символов является максимальной '+numColumn(matrixOfStr));                    

