
// Написать функцию, которая транспонирует матрицу

let matr = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];

function transposeArray(array) {
    var newArray = [];
    for (var i = 0; i < array.length; i++) {
        newArray.push([]);
    };

    for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array.length; j++) {
            newArray[j].push(array[i][j]);
        };
    };

    return newArray;
}

console.log(matr);
console.log(transposeArray(matr));
