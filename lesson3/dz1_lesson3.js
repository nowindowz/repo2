

// Все скрипты которые писали в рамках первого и второго задания - оформить в виде функций


function dz1_lesson1(b) {
  switch (b) {
    case 1:
      console.log('один');
      break;
    case 2:
      console.log('два');
      break;
    case 3:
      console.log('три');
      break;
    case 4:
      console.log('четыре');
      break;
    case 5:
      console.log('пять');
      break;
    case 6:
      console.log('шесть');
      break;
    case 7:
      console.log('семь');
      break;
    case 8:
      console.log('восемь');
      break;
    case 9:
      console.log('девять');
      break;
    default:
      console.log('ноль');
  }
}

dz1_lesson1(5);

//

function dz2_lesson1(a) {
  if (a === 0) {
    console.log("число " + a + " = 0");
  }
  else if (a < 0) {
    console.log("число " + a + " отрицательное");
  }
  else {
    console.log("число " + a + " положительное");
  };
}

dz2_lesson1(-7);

//

function dz3_lesson1(a, ed) {
  switch (ed) {
    case "KB":
      console.log(String(a * 1024) + ' Byte');
      break;
    case "MB":
      console.log(String(a * 1024 * 1024) + ' Byte');
      break;
    case "GB":
      console.log(String(a * 1024 * 1024 * 1024) + ' Byte');
      break;
    default:
      console.log(String(a) + ' Byte');
  }
}

dz3_lesson1(3847, "MB");

//


function dz4_lesson1(procent, krbody, time) {
  console.log('процентов заплатит клиент за все время ' + (krbody * procent / 100 * time));
  console.log('процентов заплатит клиент за один календарный год ' + (krbody * procent / 100));
  console.log('общее количество денежных средств выплатит за все года ' + (krbody + krbody * procent / 100 * time));
}

dz4_lesson1(15, 1000, 5);

//
function dz1_lesson2(t) {
  var res = '';

  for (var i = 0; i < t.length; i++) {
    res = t[i] + res;
  };

  console.log(res);
}
dz1_lesson2('Привет JS');

//
function dz2_lesson2(a) {

  let res = 1;
  let b = a;
  while ((b--) > 0) {
    res *= b + 1;
  }
  console.log(a + '! = ' + res);

}

dz2_lesson2(7);

//
function dz3_lesson2(a, n) {
  let i = 1;
  while (n > 0) {

    if (a % i == 0) {
      console.log(i);
      n--;
    }
    i++;
  };

}

dz3_lesson2(18, 5);

//
function dz4_lesson2(num) {
  let sum =0;
  let a = num % 10;
  while (num > 0) {
    a = num % 10;
    num = (num - a) / 10;
    if (a % 2 == 0) {
      sum += a;
    };
  }
  console.log(sum);
}

dz4_lesson2(785428);

//
function dz5_lesson2(i) {
  while ((i % 17) !=0 ) {
      i++;
  }; 
  console.log(i);
}

dz5_lesson2(301);

//
function dz6_lesson2(a, b) {
  let i =b;

  if(a <=b) {i =a};
  
  while ((a % i) !=0 || (b % i) !=0) {
      i--;
  }; 
  console.log(i);
}

dz6_lesson2(880, 200);