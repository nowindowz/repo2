var a =0;

// способ 1
if(a === 1){
    console.log('один');
}
else if(a === 2){
    console.log('два');
}
else if(a === 3){
    console.log('три');
}
else if(a === 4){
    console.log('четыре');
}
else if(a === 5){
    console.log('пять');
}
else if(a === 6){
    console.log('шесть');
}
else if(a === 7){
    console.log('семь');
}
else if(a === 8){
    console.log('восемь');
}
else if(a === 9){
    console.log('девять');
}
else {
    console.log('ноль');
};

//способ 2
var b =8;

switch (b) {
  case 1:
    console.log('один');
    break;
  case 2:
    console.log('два');
    break;
  case 3:
    console.log('три');
    break;
  case 4:
    console.log('четыре');
    break;
  case 5:
    console.log('пять');
    break;
  case 6:
    console.log('шесть');
    break;
  case 7:
    console.log('семь');
    break;
  case 8:
    console.log('восемь');
    break;
  case 9:
    console.log('девять');
    break;
  default:
    console.log('ноль');
}