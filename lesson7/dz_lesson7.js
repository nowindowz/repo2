

window.onload = function () {

    /*При каждом действии удаления или добавления студентов нужно пересчитывать статистику
    средней оценки в разрезе каждого курса и подсчета количества неактивных студентов
    и изменять соответствующее содержимое.*/

    let Students = [];

    function Student(name, estimate, course, active) {
        this.name = name;
        this.estimate = estimate;
        this.course = course;
        this.active = active;
    };

    Students.push(new Student('Ivan', 4, 1, true));
    Students.push(new Student('Ivan', 3, 1, true));
    Students.push(new Student('Ivan', 2, 4, false));
    Students.push(new Student('Ivan', 5, 2, true));

    function color(estimate){
        let color ='red';

        if (estimate > 3 && estimate <= 4){
            color ='yellow';  
        }else if(estimate > 4){
            color ='green';
        };

        return color;
    };

    function renderStudents() {

        let list = document.querySelector(".list");
        list.innerHTML = "";

        if (Students.length == 0 ){
            list.innerHTML = 'Студенты не найдены';
            return '';
        };

        let ulStudents = document.createElement("UL");
        for(let i = 0; i < Students.length; i++){
            let liStudents = document.createElement("LI");
            let liSpan = document.createElement("SPAN");
            let liSpanName = document.createElement("SPAN");
            let liSpanEstimate = document.createElement("SPAN");
            let liSpanCourse = document.createElement("SPAN");

            let buttonSt = document.createElement("BUTTON");
            buttonSt.innerHTML = 'X';

            // liSpan.innerHTML = 'name:'+Students[i].name+' course: '+Students[i].course +' estimate: '+Students[i].estimate+' active: '+Students[i].active+'  ';
            liSpanName.innerHTML = ' name: '+Students[i].name+' '; 
            liSpanEstimate.innerHTML = ' estimate: '+Students[i].estimate+' '; 
            liSpanCourse.innerHTML = ' course: '+Students[i].course+' '; 

            liSpan.style.width = '320px';
            liSpan.className = 'lispan';
            liSpanName.className = 'lispan';
            liSpanEstimate.className = 'lispan';
            liSpanCourse.className = 'lispan';
           
            // В ряде предыдущих заданий - выделять красным цветом тех студентов которые
            // имеют оценку 3 и менее. которые от 3 до 4  - желтым и которые 4 и выше - зеленым.

            let color ='red';
            
            if (Students[i].estimate > 3 && Students[i].estimate <= 4){
                color ='yellow';
                liSpanName.style.backgroundColor ='#6a9bdb';
                liSpanEstimate.style.backgroundColor ='#6a9bdb';
                liSpanCourse.style.backgroundColor ='#6a9bdb';
            }else if(Students[i].estimate > 4){
                color ='green';
            };

            liSpanName.style.color =color;
            liStudents.style.color =color;

            liStudents.appendChild(liSpanName);
            liStudents.appendChild(liSpanEstimate);
            liStudents.appendChild(liSpanCourse);


            // liStudents.appendChild(liSpan);
            liStudents.appendChild(buttonSt);

            // Добавить для каждого студента иконку по нажатию на которую студент
            // переводится в статус неактивный из активного и наоборот - 
            // при этом для двух состояний иконки тоже должны быть разными и изменять

            let pic = document.createElement("IMG");
            if (Students[i].active) {
                pic.src = "../repo2/lesson7/active.png";
            }else{
                pic.src = "../repo2/lesson7/notactive.png";
            };
            
            pic.style.height = '30px';
            pic.style.width = '30px';
            liStudents.appendChild(pic);

            (function (i){
                buttonSt.addEventListener("click", function(event){
                    Students.splice(i, 1);
                    renderStudents();
                });

                pic.addEventListener("click", function(event){
                    Students[i].active = !Students[i].active;
                    renderStudents();
                });

                // По нажатие на имя студента - удалять имя, вместо имени показывать форму ввода - 
                // по нажатию на ENTER сохранять новое имя для этого студента,
                // удалять форму ввода и выводить в списке новое имя студента

                // По аналогии предыдущего пункта сделать тоже самое с номером курса
                // и с оценкой студента. Не забыть что при изменении оценки 
                // статистика также должна быть пересчитана и выведена новая статистика.


                liSpanName.addEventListener("click", function(event){
                    let input = document.createElement("INPUT");
                    input.type = "text";
                    input.className = "input";
                    liSpanName.innerHTML = '';
                    liSpanName.appendChild(input);

                    input.addEventListener("keydown", function(event){
                        if(event.keyCode === 13){
                            Students[i].name = event.target.value;
                            renderStudents();
                        }
                     });

                     input.focus();
                });
                liSpanEstimate.addEventListener("click", function(event){
                    let inputEstimate = document.createElement("INPUT");
                    inputEstimate.type = "number";
                    inputEstimate.className = "input";
                    liSpanEstimate.innerHTML = '';
                    liSpanEstimate.appendChild(inputEstimate);

                    inputEstimate.addEventListener("keydown", function(event){
                        if(event.keyCode === 13){
                            Students[i].estimate = Number(event.target.value);
                            renderStudents();
                        }
                     });

                     inputEstimate.focus();
                });
                liSpanCourse.addEventListener("click", function(event){
                    // Students[i].сourse = 777;
                    // alert(Students[i].course);

                    let inputCourse = document.createElement("INPUT");
                    inputCourse.type = "number";
                    inputCourse.className = "input";
                    liSpanCourse.innerHTML = '';
                    liSpanCourse.appendChild(inputCourse);

                    inputCourse.addEventListener("keydown", function(event){
                        if(event.keyCode === 13){
                            // Students[i].сourse = event.target.value;
                            Students[i].course = Number(event.target.value);
                            renderStudents();
                        }
                     });

                     inputCourse.focus();
                });

            })(i);

            ulStudents.appendChild(liStudents);
        };

        list.appendChild(ulStudents);

        averageEstimate(list);

    };

    renderStudents();

    function averageEstimate(list) {

        let courseEst = {};
        let courseAverageEst = {};

        let courseArr = [];
        let notActiveStudents = {};
        let notActiveAll = 0;

        for (let i in Students) {
            if (courseArr.indexOf(Students[i].course) == -1) {
                courseArr.push(Students[i].course);
            };
        };

        for (let x = 0; x < courseArr.length; x++) {

            courseEst[courseArr[x]] = [];
            notActiveStudents[courseArr[x]] = 0;

            for (let i in Students) {
                if (Students[i].course == courseArr[x]) {
                    if (Students[i].active) {

                        courseEst[courseArr[x]].push(Students[i].estimate);

                    } else {
                        notActiveStudents[courseArr[x]] += 1;
                    };
                };
            };

            for (let key in notActiveStudents) {
                if (key == courseArr[x]) {
                    notActiveAll += notActiveStudents[key];
                };
            };

            // console.log(courseEst);

            for (let key2 in courseEst) {

                if (key2 == courseArr[x]) {

                    let courseSumEstimates = 0;
                    for (let j = 0; j < courseEst[key2].length; j++) {
                        courseSumEstimates += courseEst[key2][j];
                    };

                    if (courseEst[key2].length > 0) {
                        courseAverageEst[courseArr[x]] = courseSumEstimates / courseEst[key2].length;
                    };
                };
            };
        };

        // Аналогично как в предыдущем задании этого урока отмечать фоновым цветом 
        // вывод статистики в разрезе каждого курса касательно средней оценки

        let divEmpty = document.createElement('div');
        divEmpty.className = "alert";
        list.appendChild(divEmpty);

        let div4 = document.createElement('div');
        div4.className = "alert";
        div4.innerHTML = 'Средняя оценка по курсам: ';

        for (let index in courseAverageEst){

            let br = document.createElement('br');
            div4.appendChild(br);

            let Span = document.createElement('SPAN');
            Span.innerHTML = 'course'+index+': '+courseAverageEst[index]+' <br>';
            Span.style.backgroundColor =color(courseAverageEst[index]);
            div4.appendChild(Span);
        };

        list.appendChild(div4);

        let div5 = document.createElement('div');
        div5.className = "alert";
        div5.innerHTML = 'Не активные студенты по курсам: ';
        for (let index in notActiveStudents){
            div5.innerHTML += 'course'+index+': '+notActiveStudents[index]+' ';
        };

        list.appendChild(div5);

        let div6 = document.createElement('div');
        div6.className = "alert";
        div6.innerHTML = 'Всего не активных студентов: '+notActiveAll;
        list.appendChild(div6);

    };

    let inputCheckbox = document.createElement("INPUT");
    inputCheckbox.type = "checkbox";
    inputCheckbox.id = "checkboxActive";  
    inputCheckbox.name = "checkboxActive";
    inputCheckbox.className  = "add";

    let label = document.createElement("LABEL");
    label.setAttribute("for", "checkboxActive");
    label.innerHTML = 'Active';

    let inputName = document.createElement("INPUT");
    inputName.type = "text";
    inputName.value = "";
    inputName.name = "name";
    inputName.className  = "add";
    inputName.placeholder="name";

    let inputCourse = document.createElement("INPUT");
    inputCourse.type = "number";
    inputCourse.value = "";
    inputCourse.name = "course";
    inputCourse.className  = "add";
    inputCourse.placeholder="Course";

    let inputEstimate = document.createElement("INPUT");
    inputEstimate.type = "number";
    inputEstimate.value = "";
    inputEstimate.name = "estimate";
    inputEstimate.className  = "add";
    inputEstimate.placeholder="Estimate";

    let StudentAdd = document.getElementById("form");

    StudentAdd.appendChild(inputName);
    StudentAdd.appendChild(inputCourse);
    StudentAdd.appendChild(inputEstimate);

    StudentAdd.appendChild(inputCheckbox);
    StudentAdd.appendChild(label);

    let buttonAdd = document.createElement("BUTTON");
    buttonAdd.innerHTML = 'Добавить';
    buttonAdd.className  = "add";
    StudentAdd.appendChild(buttonAdd);

    buttonAdd.addEventListener("click", event => {

        let formData = new FormData(StudentAdd);
        let name = formData.get("name");
        let course = formData.get("course");
        let estimate = Number(formData.get("estimate"));

        let active = false;
        if(formData.get("checkboxActive") == 'on'){
            active = true;
        };

        Students.push(new Student(name, estimate, course, active));

        renderStudents();
    });

};

